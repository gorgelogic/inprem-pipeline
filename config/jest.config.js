const path = require('path');

module.exports = {
    rootDir: path.join(process.cwd(), 'test'),
    collectCoverage: true,
    verbose: true
};