const path = require('path');

module.exports = {
  context: path.join(process.cwd(), 'src'), 

  resolve: {
    extensions: ['.js'],  
    modules: [path.join(process.cwd(), 'src'), 'node_modules'] 
  },

  module: {
    rules: [{
      enforce: "pre", 
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "eslint-loader"
    }, {
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    }]
  }
};