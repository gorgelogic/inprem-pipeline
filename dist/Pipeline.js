"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _stage = require("./stage");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var registryKey = Symbol();

var Pipeline = function () {
    _createClass(Pipeline, null, [{
        key: "create",

        // Static Methods
        // The pipeliner maintains a registry of named pipelines.
        value: function create(name) {
            if (!name) throw new Error("Pipeline.create: Name required");
            if (Pipeline[registryKey].find(function (item) {
                return item.name === name;
            })) throw new Error("Pipeline: " + name + " already exists");

            var pipeline = new Pipeline(name);
            Pipeline[registryKey].push(pipeline);

            return pipeline;
        }
    }, {
        key: "delete",
        value: function _delete(name) {
            if (!name) throw new Error("Pipeline.create: Name required");

            var index = Pipeline[registryKey].findIndex(function (item) {
                return item.name === name;
            });
            if (index === -1) return false;

            Pipeline[registryKey].splice(index, 1);
            return true;
        }
    }, {
        key: "get",
        value: function get(name) {
            if (!name) throw new Error("Pipeline.create: Name required");

            return Pipeline[registryKey].find(function (item) {
                return item.name === name;
            });
        }

        // Usage:
        //      new Pipleine([name], [StageContainerType]) 
        //
        //    If name is specified, the pipeline will be saved in the registry.
        //    The container type is (ParallelStages, SerialStages)
        //    SerialStages is the default

    }]);

    function Pipeline(nameOrType, type) {
        _classCallCheck(this, Pipeline);

        var hasName = typeof nameOrType === "string";
        this.name = hasName ? nameOrType : undefined;

        var containerConstructor = hasName ? type : nameOrType;
        containerConstructor = containerConstructor || _stage.SerialStages;
        this.stages = new containerConstructor("internal");

        if (this.name) Pipeline[registryKey].push(this);
    }

    // Usage:
    //      addStage(stage)
    //      addStage(fn)
    //      addStage(name, fn)


    _createClass(Pipeline, [{
        key: "addStage",
        value: function addStage(stageNameOrFn, fn) {
            if (arguments.length === 1 && arguments[0] instanceof _stage.Stage) {
                this.stages.addStage(stageNameOrFn);
                return this;
            }

            if (arguments.length === 1 && typeof arguments[0] === "function") {
                this.stages.addStage(stageNameOrFn);
                return this;
            }

            if (arguments.length === 2 && typeof arguments[0] === "string" && typeof arguments[1] === "function") {
                this.stages.addStage(stageNameOrFn, fn);
                return this;
            }

            throw new Error("Pipeline.addStage: Improper usage");
        }
    }, {
        key: "addPipeline",
        value: function addPipeline(pipeline) {
            this.stages.addStage(new _stage.Stage("pipelineStage:" + pipeline.name, function (context, next) {
                pipeline.execute(context).then(function (value) {
                    next(value);
                });
            }));
        }
    }, {
        key: "execute",
        value: function execute(context) {
            var _this = this;

            return new Promise(function (resolve) {
                _this.stages.execute(context, function (value) {
                    return resolve(value);
                });
            });
        }
    }, {
        key: "getStage",
        value: function getStage(indexOrName) {
            return this.stages.getStage(indexOrName);
        }
    }]);

    return Pipeline;
}();

Pipeline[registryKey] = [];

exports.default = Pipeline;