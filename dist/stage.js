"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Stage = function () {
    // Usage
    //      new Stage(fn)
    //      new Stage(name, fn)
    function Stage(nameOrFn, fn) {
        _classCallCheck(this, Stage);

        var hasName = typeof nameOrFn === "string";
        this.name = hasName ? nameOrFn : undefined;
        this.fn = hasName ? fn : nameOrFn;

        if (typeof this.fn !== "function") throw new Error("Stage: fn required");
    }

    _createClass(Stage, [{
        key: "execute",
        value: function execute(context, next) {
            var _this = this;

            try {
                this.fn(context, function (value) {
                    Promise.resolve(value).then(function (value) {
                        if (typeof value === "undefined" || typeof value === "string" || value === Object(value) || value instanceof Error) {

                            next(value);
                        } else throw new Error("Invalid next value from stage " + _this.name);
                    }).catch(function (error) {
                        return next(error);
                    });
                });
            } catch (error) {
                next(error);
            }
        }
    }]);

    return Stage;
}();

/*
    Note that the two container classes (ParallelStages, SerialStages)
    are examples of the structural "composite" pattern.
    GOF: Design patterns 1995 p.163

    In short:
        The container class ISA Stage
        The container class HASA array of stages
*/

var StageContainerBase = function (_Stage) {
    _inherits(StageContainerBase, _Stage);

    function StageContainerBase(name) {
        _classCallCheck(this, StageContainerBase);

        var _this2 = _possibleConstructorReturn(this, (StageContainerBase.__proto__ || Object.getPrototypeOf(StageContainerBase)).call(this, name, function () {}));

        _this2.stages = [];
        return _this2;
    }

    _createClass(StageContainerBase, [{
        key: "addStage",


        // Usage
        //      addStage(stage)
        //      addStage(fn)
        //      addStage(name, fn)
        value: function addStage(stageNameOrFn, fn) {
            if (arguments.length === 1 && stageNameOrFn instanceof Stage) this.stages.push(stageNameOrFn);else if (arguments.length === 1 && typeof stageNameOrFn === "function") this.stages.push(new Stage(stageNameOrFn));else if (arguments.length === 2 && typeof stageNameOrFn === "string" && typeof fn === "function") {
                this.stages.push(new Stage(stageNameOrFn, fn));
            } else throw new Error("ParallelStages.addStage: improper usage");

            return this;
        }
    }, {
        key: "execute",
        value: function execute() {
            throw new Error("StageContainerBase is 'abstract'");
        }
    }, {
        key: "getStage",
        value: function getStage(indexOrName) {
            return Number.isInteger(indexOrName) ? this.stages[indexOrName] : this.stages.find(function (item) {
                return item.name === indexOrName;
            });
        }
    }, {
        key: "length",
        get: function get() {
            return this.stages.length;
        }
    }]);

    return StageContainerBase;
}(Stage);

var ParallelStages = function (_StageContainerBase) {
    _inherits(ParallelStages, _StageContainerBase);

    function ParallelStages(name) {
        _classCallCheck(this, ParallelStages);

        return _possibleConstructorReturn(this, (ParallelStages.__proto__ || Object.getPrototypeOf(ParallelStages)).call(this, name, function () {}));
    }

    _createClass(ParallelStages, [{
        key: "execute",
        value: function execute(context, next) {
            var container = this;

            var results = [];
            var aggregator = function aggregator(value) {
                results.push(value);

                if (results.length === container.stages.length) analyzer();
            };

            var analyzer = function analyzer() {
                var error = results.find(function (item) {
                    return item instanceof Error;
                });
                if (error) {
                    next(error);
                    return;
                }

                var objectResult = results.find(function (item) {
                    return item === Object(item);
                });
                next(objectResult);
            };

            this.stages.forEach(function (stage) {
                stage.execute(context, aggregator);
            });
        }
    }]);

    return ParallelStages;
}(StageContainerBase);

var SerialStages = function (_StageContainerBase2) {
    _inherits(SerialStages, _StageContainerBase2);

    function SerialStages(name) {
        _classCallCheck(this, SerialStages);

        return _possibleConstructorReturn(this, (SerialStages.__proto__ || Object.getPrototypeOf(SerialStages)).call(this, name, function () {}));
    }

    _createClass(SerialStages, [{
        key: "execute",
        value: function execute(context, next) {
            var _this5 = this;

            var container = this;

            var chain = function chain() {
                var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

                var stage = _this5.stages[index];
                stage.execute(context, function (value) {
                    if (value instanceof Error || value === Object(value)) {
                        next(value);
                        return;
                    }

                    if (typeof value === "string") {
                        var nextIndex = container.stages.findIndex(function (item) {
                            return item.name === value;
                        });
                        if (nextIndex === -1) {
                            next(new Error("SeralStages(" + container.name + "): Stage " + (stage.name || index) + " could not find named stage \"" + value + "\""));
                            return;
                        }

                        chain(nextIndex);
                        return;
                    }

                    if (typeof value === "undefined") {
                        if (index == container.stages.length - 1) next(value);else chain(++index);

                        return;
                    }
                });
            };

            if (container.stages.length) chain();
        }
    }]);

    return SerialStages;
}(StageContainerBase);

exports.Stage = Stage;
exports.ParallelStages = ParallelStages;
exports.SerialStages = SerialStages;