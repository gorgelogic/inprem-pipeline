"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Stage = exports.SerialStages = exports.Pipeline = exports.ParallelStages = undefined;

var _Pipeline = require("./Pipeline");

var _Pipeline2 = _interopRequireDefault(_Pipeline);

var _stage = require("./stage");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.ParallelStages = _stage.ParallelStages;
exports.Pipeline = _Pipeline2.default;
exports.SerialStages = _stage.SerialStages;
exports.Stage = _stage.Stage;