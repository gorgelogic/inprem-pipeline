import Pipeline from "../src/Pipeline";

describe("Pipeline CRUD Tests", () => {
	test("Pipeline.create: name required", () => {
		expect(() => { Pipeline.create(); }).toThrow();
	});

	test("Pipeline.create: name must be unique", () => {
		Pipeline.create("moe");
		expect(() => { Pipeline.create("moe"); }).toThrow();
		Pipeline.delete("moe");
	});

	test("Pipeline.create", () => {
		Pipeline.create("larry");
		const larryPipeline = Pipeline.get("larry");
		expect(larryPipeline).not.toBeNull(); 
		expect(larryPipeline.name).toBe("larry");
		Pipeline.delete("larry");
	});

	test("Pipeline.delete: name required", () => {
		expect(() => {Pipeline.delete(); }).toThrow();
	});

	test("Pipeline.delete", () => {
		expect(Pipeline.delete("curly")).toBeFalsy();
		Pipeline.create("curly");
		expect(Pipeline.delete("curly")).toBeTruthy();
	});

	test("Pipeline.get: name required", () => {
		expect(() => {Pipeline.get(); }).toThrow();
	});

	test("Pipeline.get", () => {
		expect(Pipeline.get("shemp")).toBeUndefined();
		Pipeline.create("shemp");
		const shempPipeline = Pipeline.get("shemp");
		expect(shempPipeline).not.toBeUndefined();
		expect(shempPipeline.name).toBe("shemp");
		Pipeline.delete("shemp");
	});
});