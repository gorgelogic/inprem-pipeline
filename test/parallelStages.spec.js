import { ParallelStages, Stage } from "../src/stage";

describe("Parallel Stages tests", () => {
    test("ParallelStages.addStage(stage)", () => {
        const ps = new ParallelStages("test");
        ps.addStage(new Stage("moe", () => {}));
        expect(ps.length).toBe(1);

        const stage = ps.getStage("moe");
        expect(stage).not.toBeUndefined();
        expect(stage.name).toBe("moe");
    });

    test("ParallelStages.addStage(fn)", () => {
        const ps = new ParallelStages("test");
        ps.addStage(() => {});
        expect(ps.length).toBe(1);

        const stage = ps.getStage(0);
        expect(stage).not.toBeUndefined();
        expect(stage.name).toBeUndefined();
    });

    test("ParallelStages.addStage(name, fn)", () => {
        const ps = new ParallelStages("test");
        ps.addStage("moe", () => {});
        expect(ps.length).toBe(1);

        const stage = ps.getStage("moe");
        expect(stage).not.toBeUndefined();
        expect(stage.name).toBe("moe");
    });

    test("ParallelStages.addStage(): improper usage", () => {
        const ps = new ParallelStages("test");
        expect(() => { ps.addStage("moe"); }).toThrow();
    });

    test("ParallelStages.execute: synchronous", () => {
        const ps = new ParallelStages("test");
        
        let order = [];
        const reporter = function(context, next) {
            order.push(this.name);
            next();
        };

        ps.addStage("tom", reporter);
        ps.addStage("dick", reporter);
        ps.addStage("harry", reporter);

        ps.execute({}, () => {
            expect(order.length).toBe(3);
            expect(order[0]).toBe("tom");
        });
    });

    test("ParallelStages.execute: deferred", done => {
        const ps = new ParallelStages("test");
        
        let order = [];

        const reporter = function(context, next) {
            order.push(this.name);
            next();
        };

        const deferredReporter = function(context, next) {
            new Promise(resolve => {
                next(Promise.resolve(this.name).then(value => {
                    order.push(value);
                    resolve();
                }));
            });
        };

        ps.addStage("tom", deferredReporter);
        ps.addStage("dick", reporter);
        ps.addStage("harry", reporter);

        ps.execute({}, () => {
            expect(order.length).toBe(3);
            expect(order[2]).toBe("tom");
            done();
        });
    });

    test("ParallelStages.execute: exception", () => {
        const ps = new ParallelStages("test");
        
        let order = [];
        const reporter = function(context, next) {
            if (this.name === "dick")
                throw new Error("I am no good.");

            order.push(this.name);
            next();
        };

        ps.addStage("tom", reporter);
        ps.addStage("dick", reporter);
        ps.addStage("harry", reporter);

        ps.execute({}, value => {
            expect(value instanceof Error).toBeTruthy();
            expect(order.length).toBe(2);
        });
    });

    test("ParallelStages.execute: object result", () => {
        const ps = new ParallelStages("test");
        
        let order = [];
        const reporter = function(context, next) {
            order.push(this.name);
            next(this.name === "tom" ? { answer: 42 } : undefined);
        };

        ps.addStage("tom", reporter);
        ps.addStage("dick", reporter);
        ps.addStage("harry", reporter);

        ps.execute({}, value => {
            expect(value).toEqual({ answer: 42 });
            expect(order.length).toBe(3);
        });
    });
});