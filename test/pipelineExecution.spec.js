import Pipeline from "../src/Pipeline";
import { ParallelStages, SerialStages } from "../src/stage";

describe("Pipeline Execution Tests", () => {
    test("Single Parallel Pipeline", done => {
        let order = [];

        const reporter = function(context, next) {
            order.push(this.name);
            next();
        };

        const deferredReporter = function(context, next) {
            new Promise(resolve => {
                next(Promise.resolve(this.name).then(value => {
                    order.push(value);
                    resolve();
                }));
            });
        };
        
        const p = new Pipeline(ParallelStages);
        p.addStage("tom", deferredReporter);
        p.addStage("dick", reporter);
        p.addStage("harry", reporter);

        p.execute({}).then(() => {
            expect(order.length).toBe(3);
            expect(order[2]).toBe("tom");
            done();
        });
    });

    test("Single Serial Pipeline", done => {
        let order = [];

        const reporter = function(context, next) {
            order.push(this.name);
            next();
        };

        const deferredReporter = function(context, next) {
            new Promise(resolve => {
                next(Promise.resolve(this.name).then(value => {
                    order.push(value);
                    resolve();
                }));
            });
        };
        
        const p = new Pipeline(SerialStages);
        p.addStage("tom", deferredReporter);
        p.addStage("dick", reporter);
        p.addStage("harry", reporter);

        p.execute({}).then(() => {
            expect(order.length).toBe(3);
            expect(order[0]).toBe("tom");
            done();
        });
    });

    test("Imbedded Pipeline", done => {
        let context = { count: 0 };

        const fn1 = (context, next) => {
            context.count += 10;
            next();
        };

        const p1 = new Pipeline("p1", ParallelStages);
        p1.addStage("moe", fn1);
        p1.addStage("larry", fn1);
        p1.addStage("curly", fn1);
        p1.addStage("shemp", fn1);

        const fn2 = function(context, next) {
            ++context.count;
            next(this.name === "harry" ? { result: context.count } : undefined );
        };

        const p2 = new Pipeline("p2");
        p2.addStage("tom", fn2);
        p2.addPipeline(Pipeline.get("p1"));
        p2.addStage("dick", fn2);
        p2.addStage("harry", fn2);

        Pipeline.get("p2").execute(context).then(value => {
            expect(value.result).toBe(43);

            Pipeline.delete("p1");
            Pipeline.delete("p2");
            done();
        });
    });

    test("Pipeline Chaining", done => {
        let context = { count: 0 };

        const fn1 = function(context, next) {
            context.count += 10;
            next();
        };

        const p1 = new Pipeline("p1", ParallelStages);
        p1.addStage("moe", fn1);
        p1.addStage("larry", fn1);
        p1.addStage("curly", fn1);
        p1.addStage("shemp", fn1);

        const fn2 = function(context, next) {
            ++context.count;
            next(this.name === "harry" ? { result: context.count } : undefined );
        };

        const p2 = new Pipeline("p2");
        p2.addStage("tom", fn2);

        const p3 = new Pipeline("p3", SerialStages);
        p3.addStage("dick", fn2);
        p3.addStage("harry", fn2);

        Pipeline.get("p2").execute(context)
            .then(() => Pipeline.get("p1").execute(context))
            .then(() => Pipeline.get("p3").execute(context))
            .then(value => {
                expect(value.result).toBe(43);
                
                Pipeline.delete("p1");
                Pipeline.delete("p2");
                Pipeline.delete("p3");
                done();
            });
    });
});