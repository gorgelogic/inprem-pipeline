import { SerialStages, Stage } from "../src/stage";

describe("Serial Stages tests", () => {
    test("SerialStages.addStage(stage)", () => {
        const ps = new SerialStages("test");
        ps.addStage(new Stage("moe", () => {}));
        expect(ps.length).toBe(1);

        const stage = ps.getStage("moe");
        expect(stage).not.toBeUndefined();
        expect(stage.name).toBe("moe");
    });

    test("SerialStages.addStage(fn)", () => {
        const ps = new SerialStages("test");
        ps.addStage(() => {});
        expect(ps.length).toBe(1);

        const stage = ps.getStage(0);
        expect(stage).not.toBeUndefined();
        expect(stage.name).toBeUndefined();
    });

    test("SerialStages.addStage(name, fn)", () => {
        const ps = new SerialStages("test");
        ps.addStage("moe", () => {});
        expect(ps.length).toBe(1);

        const stage = ps.getStage("moe");
        expect(stage).not.toBeUndefined();
        expect(stage.name).toBe("moe");
    });

    test("SerialStages.addStage(): improper usage", () => {
        const ps = new SerialStages("test");
        expect(() => { ps.addStage("moe"); }).toThrow();
    });

    test("SerialStages.execute: synchronous", () => {
        const ps = new SerialStages("test");
        
        let order = [];
        const reporter = function(context, next) {
            order.push(this.name);
            next();
        };

        ps.addStage("tom", reporter);
        ps.addStage("dick", reporter);
        ps.addStage("harry", reporter);

        ps.execute({}, () => {
            expect(order.length).toBe(3);
            expect(order[0]).toBe("tom");
        });
    });

    test("SerialStages.execute: deferred", done => {
        const ps = new SerialStages("test");
        
        let order = [];

        const reporter = function(context, next) {
            order.push(this.name);
            next();
        };

        const deferredReporter = function(context, next) {
            new Promise(resolve => {
                next(Promise.resolve(this.name).then(value => {
                    order.push(value);
                    resolve();
                }));
            });
        };

        ps.addStage("tom", deferredReporter);
        ps.addStage("dick", reporter);
        ps.addStage("harry", reporter);

        ps.execute({}, () => {
            expect(order.length).toBe(3);
            expect(order[0]).toBe("tom");
            done();
        });
    });

    test("SerialStages.execute: exception",() => {
        const ps = new SerialStages("test");
        
        let order = [];
        const reporter = function(context, next) {
            if (this.name === "dick")
                throw new Error("I am no good.");

            order.push(this.name);
            next();
        };

        ps.addStage("tom", reporter);
        ps.addStage("dick", reporter);
        ps.addStage("harry", reporter);

        ps.execute({}, value => {
            expect(value instanceof Error).toBeTruthy();
            expect(order.length).toBe(1);
        });
    });

    test("SerialStages.execute: stage navigation", () => {
        const ps = new SerialStages("test");
        
        let order = [];
        const reporter = function(context, next) {
            order.push(this.name);
            next(this.name === "tom" ? "harry" : undefined);
        };

        ps.addStage("tom", reporter);
        ps.addStage("dick", reporter);
        ps.addStage("harry", reporter);

        ps.execute({}, () => {
            expect(order.length).toBe(2);
        });
    });

    test("SerialStages.execute: bad stage navigation", () => {
        const ps = new SerialStages("test");
        
        let order = [];
        const reporter = function(context, next) {
            order.push(this.name);
            next(this.name === "tom" ? "bogus" : undefined);
        };

        ps.addStage("tom", reporter);
        ps.addStage("dick", reporter);
        ps.addStage("harry", reporter);

        ps.execute({}, value => {
            expect(value instanceof Error).toBeTruthy();
            expect(order.length).toBe(1);
        });
    });

    test("SerialStages.execute: object result", () => {
        const ps = new SerialStages("test");
        
        let order = [];
        const reporter = function(context, next) {
            order.push(this.name);
            next(this.name === "tom" ? { answer: 42 } : undefined);
        };

        ps.addStage("tom", reporter);
        ps.addStage("dick", reporter);
        ps.addStage("harry", reporter);

        ps.execute({}, value => {
            expect(value).toEqual({ answer: 42 });
            expect(order.length).toBe(1);
        });
    });
});