import Pipeline from "../src/Pipeline";
import { ParallelStages, SerialStages } from "../src/stage";

describe("Pipeline constructor tests", () => {
    test("Pipeline.constructor()", () => {
        const p = new Pipeline();
        expect(p.name).toBeUndefined();
        expect(p.stages instanceof SerialStages).toBeTruthy();
    });

    test("Pipeline.constructor(name)", () => {
        const p = new Pipeline("moe");
        expect(p.name).toBe("moe");
        expect(p.stages instanceof SerialStages).toBeTruthy();

        expect(Pipeline.get("moe")).not.toBeUndefined();
        Pipeline.delete("moe");
    });

    test("Pipeline.constructor(type)", () => {
        const p = new Pipeline(ParallelStages);
        expect(p.name).toBeUndefined();
        expect(p.stages instanceof ParallelStages).toBeTruthy();
    });

    test("Pipeline.constructor(name, type)", () => {
        const p = new Pipeline("moe", ParallelStages);
        expect(p.name).toBe("moe");
        expect(p.stages instanceof ParallelStages).toBeTruthy();

        expect(Pipeline.get("moe")).not.toBeUndefined();
        Pipeline.delete("moe");
    });
});