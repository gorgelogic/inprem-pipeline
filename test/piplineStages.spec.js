import Pipeline from "../src/Pipeline";
import { Stage } from "../src/stage";

describe("Pipeline stages tests", () => {
    test("addStage('stage')", () => {
        const pipeline = new Pipeline();
        pipeline.addStage(new Stage("moe", () => {}));
        expect(pipeline.stages.length).toBe(1);

        const stage = pipeline.getStage(0);
        expect(stage.name).toBe("moe");
    });

    test("addStage(fn)", () => {
        const pipeline = new Pipeline();
        pipeline.addStage(() => {});
        expect(pipeline.stages.length).toBe(1);

        const stage = pipeline.getStage(0);
        expect(stage.name).toBeUndefined();
    });

    test("addStage(name, fn)", () => {
        const pipeline = new Pipeline();
        pipeline.addStage("moe", () => {});
        expect(pipeline.stages.length).toBe(1);

        const stage = pipeline.getStage(0);
        expect(stage.name).toBe("moe");
    });

    test("addStage error", () => {
        const pipeline = new Pipeline();
        expect(() => {
            pipeline.addStage("moe", new Stage(() => {}));
        }).toThrow();
    });
});