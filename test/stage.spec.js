import { Stage } from "../src/stage";

describe("Stage tests", () => {
    test("Stage.constructor(fn)", () => {
        const stage = new Stage(() => {});
        expect(stage.name).toBeUndefined();
        expect(typeof stage.fn).toBe("function");
    });

    test("Stage.constructor(name, fn)", () => {
        const stage = new Stage("moe", () => {});
        expect(stage.name).toBe("moe");
        expect(typeof stage.fn).toBe("function");
    });

    test("Stage.constructor: bad usage", () => {
        expect(() => new Stage("this", "is", "bogus")).toThrow();
    });

    test("Stage execution: normal(undefined)", () => {
        const stage = new Stage("synchronous", (context, next) => {
            expect(context).toEqual({ context: true });
            next();
        });

        stage.execute({ context: true}, value => {
            expect(value).toBeUndefined();
        });
    });

    test("Stage execution: normal(string)", () => {
        const stage = new Stage("synchronous", (context, next) => {
            expect(context).toEqual({ context: true });
            next("Hello");
        });

        stage.execute({ context: true}, value => {
            expect(value).toBe("Hello");
        });
    });

    test("Stage execution: normal(object)", () => {
        const stage = new Stage("synchronous", (context, next) => {
            expect(context).toEqual({ context: true });
            next({ answer: 42 });
        });

        stage.execute({ context: true}, value => {
            expect(value).toEqual({ answer: 42 });
        });
    });

    test("Stage execution: exception", () => {
        const stage = new Stage("synchronous", () => {
            throw new Error("Whoops!");
        });

        stage.execute({}, value => {
            expect(value instanceof Error).toBeTruthy();
        });
    });

    test("Stage execution: illegal value", () => {
        const stage = new Stage("synchronous",(context, next) => {
            next(NaN);
        });

        stage.execute({}, value => {
            expect(value instanceof Error).toBeTruthy();
        });
    });

    test("Stage execution: deferred(undefined)", done => {
        const stage = new Stage("synchronous", (context, next) => {
            next(Promise.resolve());
        });

        stage.execute({ context: true}, value => {
            expect(value).toBeUndefined();
            done();
        });
    });

    test("Stage execution: deferred(string)", done => {
        const stage = new Stage("synchronous", (context, next) => {
            next(Promise.resolve("Hello"));
        });

        stage.execute({ context: true}, value => {
            expect(value).toBe("Hello");
            done();
        });
    });

    test("Stage execution: deferred(object)", done => {
        const stage = new Stage("synchronous", (context, next) => {
            next(Promise.resolve({ answer: 42 }));
        });

        stage.execute({ context: true}, value => {
            expect(value).toEqual({ answer: 42 });
            done();
        });
    });

    test("Stage execution: deferred exception", done => {
        const stage = new Stage("synchronous", (context, next) => {
            next(new Promise(() => {
                throw new Error("Oh No!");
            }));
        });

        stage.execute({ context: true}, value => {
            expect(value instanceof Error).toBeTruthy();
            done();
        });
    });
});