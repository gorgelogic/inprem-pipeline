class Stage {
    // Usage
    //      new Stage(fn)
    //      new Stage(name, fn)
    constructor(nameOrFn, fn) {
        const hasName = typeof nameOrFn === "string";
        this.name = hasName ? nameOrFn : undefined;
        this.fn = hasName ? fn : nameOrFn;

        if (typeof this.fn !== "function")
            throw new Error("Stage: fn required");
    }

    execute(context, next) {
        try {
            this.fn(context, value => {
                Promise.resolve(value).then(value => {
                    if (typeof value === "undefined" ||
                        typeof value === "string" ||
                        value === Object(value) ||
                        value instanceof Error) {

                        next(value);
                    } else
                        throw new Error(`Invalid next value from stage ${this.name}`);
                }).catch(error => next(error));
            });
        } catch(error) {
            next(error);
        }
    }
}

/*
    Note that the two container classes (ParallelStages, SerialStages)
    are examples of the structural "composite" pattern.
    GOF: Design patterns 1995 p.163

    In short:
        The container class ISA Stage
        The container class HASA array of stages
*/

class StageContainerBase extends Stage {
    constructor(name) {
        super(name, () => {});
        this.stages = [];
    }

    get length() {
        return this.stages.length;
    }

    // Usage
    //      addStage(stage)
    //      addStage(fn)
    //      addStage(name, fn)
    addStage(stageNameOrFn, fn) {
        if (arguments.length === 1 && stageNameOrFn instanceof Stage)
            this.stages.push(stageNameOrFn);
        else if (arguments.length === 1 && typeof stageNameOrFn === "function")
            this.stages.push(new Stage(stageNameOrFn));
        else if (arguments.length === 2 &&
                 typeof stageNameOrFn === "string" &&
                 typeof fn === "function") {
            this.stages.push(new Stage(stageNameOrFn, fn));
        }
        else
            throw new Error("ParallelStages.addStage: improper usage");
            
        return this;
    }

    execute() {
        throw new Error("StageContainerBase is 'abstract'");
    }

    getStage(indexOrName) {
        return Number.isInteger(indexOrName)
            ? this.stages[indexOrName]
            : this.stages.find(item => item.name === indexOrName);
    }
}

class ParallelStages extends StageContainerBase {
    constructor(name) {
        super(name, () => {});
    }

    execute(context, next) {
        const container = this;

        let results = [];
        const aggregator = value => {
            results.push(value);

            if (results.length === container.stages.length) 
                analyzer();
        };

        const analyzer = () => {
            const error = results.find(item => item instanceof Error);
            if (error) {
                next(error);
                return;
            }

            const objectResult = results.find(item => item === Object(item));
            next(objectResult);
        };

        this.stages.forEach(stage => {
            stage.execute(context, aggregator);
        });
    }
}

class SerialStages extends StageContainerBase {
    constructor(name) {
        super(name, () => {});
    }

    execute(context, next) {
        const container = this;

        const chain = (index = 0) => {
            const stage = this.stages[index];
            stage.execute(context, value => {
                if (value instanceof Error || value === Object(value)) {
                    next(value);
                    return;
                }

                if (typeof value === "string") {
                    const nextIndex = container.stages.findIndex(item => item.name === value);
                    if (nextIndex === -1) {
                        next(new Error(`SeralStages(${container.name}): Stage ${stage.name || index} could not find named stage "${value}"`));
                        return;
                    }

                    chain(nextIndex);
                    return;
                }
                
                if (typeof value === "undefined") {
                    if (index == container.stages.length - 1)
                        next(value);
                    else
                        chain(++index);

                    return;
                }
            });
        };

        if (container.stages.length)
            chain();
    }
}

export { Stage, ParallelStages, SerialStages };