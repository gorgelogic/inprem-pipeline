import { Stage, SerialStages } from "./stage";

const registryKey = Symbol();

class Pipeline {
    // Static Methods
    // The pipeliner maintains a registry of named pipelines.
    static create(name) {
        if (!name)
            throw new Error("Pipeline.create: Name required");
        if (Pipeline[registryKey].find(item => item.name === name))
            throw new Error(`Pipeline: ${name} already exists`);

        const pipeline = new Pipeline(name);
        Pipeline[registryKey].push(pipeline);

        return pipeline;
    }

    static delete(name) {
        if (!name)
            throw new Error("Pipeline.create: Name required");

        const index = Pipeline[registryKey].findIndex(item => item.name === name);
        if (index === -1)
            return false;

        Pipeline[registryKey].splice(index, 1);
        return true;
    }

    static get(name) {
        if (!name)
            throw new Error("Pipeline.create: Name required");

        return Pipeline[registryKey].find(item => item.name === name);
    }

    // Usage:
    //      new Pipleine([name], [StageContainerType]) 
    //
    //    If name is specified, the pipeline will be saved in the registry.
    //    The container type is (ParallelStages, SerialStages)
    //    SerialStages is the default
    constructor(nameOrType, type) {
        const hasName = typeof nameOrType === "string";
        this.name = hasName ? nameOrType : undefined;

        let containerConstructor = hasName ? type : nameOrType;
        containerConstructor = containerConstructor || SerialStages;
        this.stages = new containerConstructor("internal");

        if (this.name)
            Pipeline[registryKey].push(this);
    }


    // Usage:
    //      addStage(stage)
    //      addStage(fn)
    //      addStage(name, fn)
    addStage(stageNameOrFn, fn) {
        if (arguments.length === 1 && arguments[0] instanceof Stage) {
            this.stages.addStage(stageNameOrFn);
            return this;
        }

        if (arguments.length === 1 && typeof arguments[0] === "function") {
            this.stages.addStage(stageNameOrFn);
            return this;
        }

        if (arguments.length === 2 && typeof arguments[0] === "string" && typeof arguments[1] === "function") {
            this.stages.addStage(stageNameOrFn, fn);
            return this;
        }

        throw new Error("Pipeline.addStage: Improper usage");
    }

    addPipeline(pipeline) {
        this.stages.addStage(new Stage(`pipelineStage:${pipeline.name}`, (context, next) => {
            pipeline.execute(context).then(value => {
                next(value);
            });
        }));
    }

    execute(context) {
        return new Promise(resolve => {
            this.stages.execute(context, value => resolve(value));
        });
    }

    getStage(indexOrName) {
        return this.stages.getStage(indexOrName);
    }
}

Pipeline[registryKey] = [];

export default Pipeline;